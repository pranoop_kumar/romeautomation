package com.Pranoop.RomeAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RomeHomePage {
	
	

    private WebDriver driver;
//	
//	@FindBy(how = How.ID, using = "j_s_sctrl_tabScreen")
//	 
//	public WebElement dropdown_button;
	
	

	public OpportunityPage selectFromDropDownByValue(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
        Select select = new Select(driver.findElement(By.id("j_s_sctrl_tabScreen")));
		//Select select = new Select(dropdown_button);
		select.selectByValue("tabScreen32");
		
		Thread.sleep(30000);
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("s_2_1_11_0_Ctrl")));
		return PageFactory.initElements(driver, OpportunityPage.class);
				
		
	}
	
	public RomeHomePage(WebDriver driver)
	{
		this.driver = driver;
		WebDriverWait wait1 = new WebDriverWait(driver,60);
		wait1.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("j_s_sctrl_tabScreen"))));
	}
	
	

}
