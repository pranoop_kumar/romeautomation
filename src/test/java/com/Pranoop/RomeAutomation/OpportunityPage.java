package com.Pranoop.RomeAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OpportunityPage {

	public QuickCreatePage quickCreate(WebDriver driver) {
		// TODO Auto-generated method stub
		driver.findElement(By.id("s_2_1_11_0_Ctrl")).click();
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[3]/td[4]/div/input")));
		
		return PageFactory.initElements(driver, QuickCreatePage.class);
	}

}
