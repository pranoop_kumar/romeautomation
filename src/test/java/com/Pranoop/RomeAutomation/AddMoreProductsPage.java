package com.Pranoop.RomeAutomation;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddMoreProductsPage {

	public void selectMoreProductsFromDropdown(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		//Select No from Add more products dropdown
		
		driver.findElement(By.id("s_1_1_86_0_icon")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/ul/li[1]")).click(); // Select No
		Thread.sleep(1000);
	}

	public OptyDetailsPage clickOnFinish(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		driver.findElement(By.id("s_1_1_5_0_Ctrl")).click(); // click on Finish

		WebDriverWait wait = new WebDriverWait(driver,30);
		
		wait.until(ExpectedConditions.alertIsPresent());
		//String windowHandle=driver.getWindowHandle();
		
	//	Thread.sleep(10000);
		Alert alt = driver.switchTo().alert();
		alt.accept();
	
        
		Thread.sleep(20000);
		//driver.switchTo().window(windowHandle);
//		driver.switchTo().defaultContent();
	    WebDriverWait wait1 = new WebDriverWait(driver,30);
        wait1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/div[3]/div/div/table/tbody/tr[3]/td[3]/div/input")));
		return PageFactory.initElements(driver,OptyDetailsPage.class);
		
	}

}
