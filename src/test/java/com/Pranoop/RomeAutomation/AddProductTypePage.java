package com.Pranoop.RomeAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddProductTypePage {

	public AddProductDetailsPage selectproducttype(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		
		driver.findElement(By.id("s_1_1_9_0_icon")).click();
		
		Thread.sleep(2000);
//		WebDriverWait wait = new WebDriverWait(driver,60);
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/ul/li[2]")));
		driver.findElement(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/ul/li[2]")).click(); // Select Wireline product
		driver.findElement(By.id("s_1_1_3_0_Ctrl")).click(); // Click on Next
		
		WebDriverWait wait1 = new WebDriverWait(driver,60);
		wait1.until(ExpectedConditions.elementToBeClickable(By.id("s_1_1_11_0_icon")));
		
		return PageFactory.initElements(driver,AddProductDetailsPage.class);
		
	}

}
