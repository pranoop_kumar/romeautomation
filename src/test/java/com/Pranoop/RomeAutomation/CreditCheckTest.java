package com.Pranoop.RomeAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.Pranoop.Util.WebUtil;

public class CreditCheckTest {
	
	 WebDriver driver = new FirefoxDriver();
	
	 
  @Test
  public void f() throws InterruptedException {
	  
	 
       //     1. Go to Sign in page Rome
 	  
		
	  
	        SignInPage signInPage = WebUtil.goToSignInPage(driver);
	        driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			signInPage.fillInUserName(driver,"ec006e");
			signInPage.fillPassword(driver,"Rome2015");
			
			//2. Navigate to ROME home Page
			
			RomeHomePage romeHomePage = signInPage.clickSignin(driver);
			
			//3. Select opportunity from the drop down	and Navigate to Opportunity Page  
			
	      OpportunityPage optyHomePage =  romeHomePage.selectFromDropDownByValue(driver);
			
	      // 4. Click on Quick Create and navigate to ABS quick create opty page
			QuickCreatePage quickcreatepage = optyHomePage.quickCreate(driver);
	      
			 //6 . Fill details on Quick create page 
//			WebDriverWait wait1 = new WebDriverWait(driver, 30);
//			 wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[3]/td[4]/div/input")));
			Thread.sleep(8000);
			quickcreatepage.filloptyname(driver);
			
			 quickcreatepage.fillsubaccountname(driver);
			
			quickcreatepage.filloptytype(driver);
		
			quickcreatepage.fillclosedate(driver);
		
			AddProductTypePage addproducttypepage =  quickcreatepage.clickaddproducts(driver);
			
			AddProductDetailsPage addproductdetailspage = addproducttypepage.selectproducttype(driver);
			
			
			addproductdetailspage.searchproductname(driver);
			
			addproductdetailspage.giveproductdetails(driver);
			
			addproductdetailspage.selectoffertype(driver);
			
			addproductdetailspage.selectoffertype(driver);
			
			addproductdetailspage.selectcontractterm(driver);
			
			addproductdetailspage.selectquantity(driver);
			
			addproductdetailspage.fillrecurringrevenue(driver);
			addproductdetailspage.fillnonrecurringrevenue(driver);
			
			AddMoreProductsPage addmoreproductspage = addproductdetailspage.clickNext(driver);
			
			addmoreproductspage.selectMoreProductsFromDropdown(driver);			
			
			OptyDetailsPage optydetailspage = addmoreproductspage.clickOnFinish(driver);
			
			optydetailspage.findOptyId(driver);
			
			
			String optyId = driver.findElement(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/div[3]/div/div/table/tbody/tr[3]/td[3]/div/input")).getText();
			System.out.println(optyId);
			System.out.println(driver.findElement(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/div/div[1]/div/div[1]/div/form/div/span/div[1]/div[2]/span/label")).getText());
			Assert.assertTrue(true,optyId);
	  
  }
}
