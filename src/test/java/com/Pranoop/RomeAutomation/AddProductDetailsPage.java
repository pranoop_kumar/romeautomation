package com.Pranoop.RomeAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddProductDetailsPage {

	public void searchproductname(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		
		driver.findElement(By.id("s_1_1_11_0_icon")).click(); // Click on Search for product name
		driver.findElement(By.xpath("html/body/div[8]/div[2]/div/div/div/form/div/table/tbody/tr/td[2]/span[4]/input")).sendKeys("AT&T VPN");
		Thread.sleep(3000);
		driver.findElement(By.id("s_2_1_1464_0_Ctrl")).click();
		Thread.sleep(3000);
	//	driver.findElement(By.xpath("html/body/div[8]/div[2]/div/div/div/form/div/div[1]/div/div/div[3]/div[3]/div/table/tbody/tr[2]/td[2]/input")).click();
		// Selected AVPN product
		driver.findElement(By.id("1_s_2_l_ABS_Product_Name")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("s_2_1_1465_0_Ctrl")).click(); // Click on OK
		Thread.sleep(1000);
		
	}

	public void giveproductdetails(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		// Give product details
					driver.findElement(By.id("s_1_1_12_0_icon")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("1_Value")).click();
					Thread.sleep(1000);
					driver.findElement(By.id("s_3_2_1472_0_icon")).click(); // T-35 Fiber option dropdown click
					Thread.sleep(1000);
					driver.findElement(By.xpath("html/body/div[8]/ul/li[1]")).click(); // select AVPN from dropdown
					Thread.sleep(2000);
					driver.findElement(By.id("2_s_3_l_Value")).click(); // Select row for T-35 fiber sale
					Thread.sleep(2000);
					driver.findElement(By.id("s_3_2_1472_0_icon")).click(); // Click on dropdown button
					Thread.sleep(1000);
					driver.findElement(By.xpath("html/body/div[8]/ul/li[1]")).click(); // Select No
					Thread.sleep(2000);
					driver.findElement(By.id("s_3_1_1463_0_Ctrl")).click();
					Thread.sleep(2000);
		
	}

	public void selectoffertype(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		//Click on offer Type drop down and select
		driver.findElement(By.id("s_1_1_14_0_icon")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/ul[2]/li[2]")).click(); // select Standalone
		
		Thread.sleep(1000);
		
	}

	public void selectcontractterm(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		
		// Select Contract Term
		
		
					driver.findElement(By.id("s_1_1_15_0_icon")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("html/body/div[1]/div/div[5]/div/div[6]/ul[3]/li[3]")).click(); // Select 24 months
					Thread.sleep(2000);
	}

	public void selectquantity(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub

		//Select Quantity
		driver.findElement(By.name("s_1_1_20_0")).sendKeys("1");
		Thread.sleep(1000);
	}

	public void fillrecurringrevenue(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		driver.findElement(By.name("s_1_1_32_0")).clear();
		driver.findElement(By.name("s_1_1_32_0")).sendKeys("20000"); // recurring revenue
		Thread.sleep(1000);
		
	}

	public void fillnonrecurringrevenue(WebDriver driver) throws InterruptedException {
	
		// TODO Auto-generated method stub
		driver.findElement(By.name("s_1_1_33_0")).clear();
		driver.findElement(By.name("s_1_1_33_0")).sendKeys("20000"); // Non-recurring revenue
		
		Thread.sleep(2000);
	}

	public AddMoreProductsPage clickNext(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		driver.findElement(By.id("s_1_1_3_0_Ctrl")).click(); // click on Next to navigate to next screen
		
	//	Thread.sleep(3000);
		
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("s_1_1_86_0_icon")));
		
		return PageFactory.initElements(driver, AddMoreProductsPage.class);
	}
	
	

}
