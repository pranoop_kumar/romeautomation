package com.Pranoop.RomeAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QuickCreatePage {

	public void filloptyname(WebDriver driver) {
		// TODO Auto-generated method stub
		
		driver.findElement(By.xpath("//tr[3]/td[4]/div/input")).sendKeys("263629_REGS54");
		System.out.println("Clicked on opty name");
	}

	public void fillsubaccountname(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		
		driver.findElement(By.id("s_1_1_27_0_icon")).click();
	    WebDriverWait wait = new WebDriverWait(driver, 30);
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("s_2_1_49_0_Ctrl")));

	//	Thread.sleep(2000);
		driver.findElement(By.id("s_2_1_49_0_Ctrl")).click();
		Thread.sleep(2000);
		driver.findElement(By.name("s_2_1_52_0")).sendKeys("3006EI");
		driver.findElement(By.id("s_2_1_53_0_Ctrl")).click();
		Thread.sleep(3000);
	}

	public void filloptytype(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		driver.findElement(By.id("s_1_1_30_0_icon")).click();
		
		Thread.sleep(3000);
       
		driver.findElement(By.xpath("//HTML/BODY/DIV[1]/DIV/DIV[5]/DIV/DIV[6]/UL[1]/LI[1]")).click();
		Thread.sleep(1000);
		
	}

	public void fillclosedate(WebDriver driver) {
		// TODO Auto-generated method stub
		driver.findElement(By.name("s_1_1_36_0")).sendKeys("2/7/2016");
	}

	public AddProductTypePage clickaddproducts(WebDriver driver) throws InterruptedException {
		// TODO Auto-generated method stub
		
		driver.findElement(By.id("s_1_1_41_0_Ctrl")).click();
		
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 60);
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("s_1_1_9_0_icon")));
	   
	    
	    return PageFactory.initElements(driver, AddProductTypePage.class);
		
	}

}
