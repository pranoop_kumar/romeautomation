package com.Pranoop.Util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.Pranoop.RomeAutomation.SignInPage;

public class WebUtil {

	public static SignInPage goToSignInPage(WebDriver driver) {
		// TODO Auto-generated method stub
		driver.get("http://rome-uat1.it.att.com:8880/romesso/eCRMLoginST.jsp");
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		return PageFactory.initElements(driver, SignInPage.class);
	}

}
